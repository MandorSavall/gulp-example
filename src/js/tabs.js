class Tab {
    /**
     * @param  {Tabs}   tabs   Instance of Tabs which contains this tab
     * @param  {Dom}    toggle toggle button
     * @param  {Dom}    tab    block to hide or show
     */
    constructor (tabs, toggle, tab) {
        this.tabs = tabs;
        this.toggle = toggle;
        this.tab = tab;
        this.src = this.tab.getAttribute('data-src');
        if (this.src !== null) {
            this.hasToBeLoaded = true;
        }

        if (this.toggle.classList.contains(this.tabs.activeToggleClassName)) {
            this.open();
        } else {
            this.close();
        }
        this.init();
    }

    init () {
        this.open = this.open.bind(this); // needed for removeEventListener
        this.toggle.addEventListener('click', this.open);
    }

    load () {
        // @todo: use fetch() function
        let xhr = new XMLHttpRequest();
        this.hasToBeLoaded = false;
        xhr.open('GET', encodeURI(this.src));
        xhr.onload = () => {
            if (xhr.status === 200 || xhr.status === 304) {
                this.tab.innerHTML = xhr.responseText;
            } else {
                this.hasToBeLoaded = true;
            }
        };
        xhr.onerror = (error) => {
            console.error(error);
        };
        xhr.send();
    }

    open () {
        if (this.tabs.active === this) {
            // already open
            return;
        }
        if (this.hasToBeLoaded) {
            this.load();
        }
        if (this.tabs.active) {
            this.tabs.active.close();
        }
        this.tabs.active = this;
        this.tab.style.display = `block`;
        this.toggle.classList.add(this.tabs.activeToggleClassName);
    }

    close () {
        this.tab.style.display = `none`;
        this.toggle.classList.remove(this.tabs.activeToggleClassName);
    }

    destroy () {
        this.toggle.removeEventListener('click', this.open);
    }
}

